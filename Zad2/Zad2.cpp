// OpenGL_rysowanie_ksztaltow_ConsoleApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

///////////////////////////////////////////////////////////
// Wywo�ywana w celu przerysowania sceny
void RenderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLES);

	glVertex2f(15.64f,  12.47);
	glVertex2f(19.5,-4.45);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(19.5, -4.45);
	glVertex2f(8.68, -18.02 );
	glVertex2f(0.0f, 0.0f);

	glVertex2f(8.68, -18.02);
	glVertex2f(-8.68, -18.02);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(-8.68, -18.02);
	glVertex2f(-19.5, -4.45);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(-19.5, -4.45);
	glVertex2f(-15.64, 12.47);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(-15.64, 12.47);
	glVertex2f(0, 20);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(0, 20);
	glVertex2f(15.64f, 12.47);
	glVertex2f(0.0f, 0.0f);

	glEnd(); 
	glFlush();
}
///////////////////////////////////////////////////////////
// Konfiguracja stanu renderowania  
void SetupRC(void) {
	// Ustalenie niebieskiego koloru czyszczcego     
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
}
///////////////////////////////////////////////////////////
// Wywo�ywana przez bibliotek GLUT w przypadku zmiany rozmiaru okna
void ChangeSize(int w, int h) {
	GLfloat aspectRatio;
	// Zabezpieczenie przed dzieleniem przez zero  
	if (h == 0)   h = 1;
	// Ustawienie wielko�ci widoku na r�wn� wielko�ci okna     
	glViewport(0, 0, w, h);
	// Ustalenie uk�adu wsp�rz�dnych  
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// Wyznaczenie przestrzeni ograniczaj�cej (lewy, prawy, dolny, g�rny, bliski, odleg�y)  
	aspectRatio = (GLfloat)w / (GLfloat)h;
	if (w <= h)    glOrtho(-100.0, 100.0, -100 / aspectRatio, 100.0 / aspectRatio, 1.0, -1.0);
	else    glOrtho(-100.0 * aspectRatio, 100.0 * aspectRatio, -100.0, 100.0, 1.0, -1.0);
	glMatrixMode(GL_MODELVIEW);  glLoadIdentity();
}
///////////////////////////////////////////////////////////
// G��wny punkt wejcia programu
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Zad2");
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ChangeSize);
	SetupRC();
	glutMainLoop();
	return 0;
}
