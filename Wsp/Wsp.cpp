#include "stdafx.h"
#include "cmath"
#include "iostream"
using namespace std;


double x(double r,double alfa) {
	return r*sin(alfa);
}

double y(double r, double alfa) {
	return r*cos(alfa);
}

int main()
{
	int angle = 7;
	double Pi = 3.14159265359;
	double step = (2*Pi) / angle;

	// Wyliczenie wsp�rz�dnych wierzcho�k�w wielok�ta

	for (double i = 0; i <= 2 * Pi; i+= step) {

		cout << "x= " << x(20, i)<<endl;
		cout << "y= " << y(20, i) << endl;

	}

	system("Pause");
    return 0;
}

