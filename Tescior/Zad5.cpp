#include "stdafx.h"
#include <vector>
#include <iostream>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
using namespace std;

GLsizei wh = 500, ww = 500;
GLfloat sizee = 4.0f;
struct point2d
{
	int x;
	int y;
};
std::vector<point2d> points;

int avarage(int axis)
{
	int max, min,avg;
	if (axis == 1) // Po X
	{
		max = min = points[0].x;
		for each (point2d var in points)
		{
			if (var.x > max) max = var.x;
			if (var.x < min) min = var.x;
		}
	}
	if (axis == 2) // Po Y
	{
		max = min = points[0].y;
		for each (point2d var in points)
		{
			if (var.y > max) max = var.y;
			if (var.y < min) min = var.y;
		}
	}

	avg = (min - max) / 2;
	if (avg < 0) avg = avg*(-1);

	avg = min + avg;
	return avg;
}

void sort(vector<point2d> & grupa, int nrGrupy)
{

	for each (point2d var in grupa)
	{
		for (int j = 0; j < grupa.size()-1; j++)
		{
			if (nrGrupy == 1 || nrGrupy == 4) if (grupa[j].x > grupa[j + 1].x)
			{
				point2d tmp = grupa[j];
				grupa[j] = grupa[j + 1];
				grupa[j + 1] = tmp;
			}
			if (nrGrupy == 2 || nrGrupy == 3) if (grupa[j].x < grupa[j + 1].x)
			{
				point2d tmp = grupa[j];
				grupa[j] = grupa[j + 1];
				grupa[j + 1] = tmp;
			}
		}
	}
}

void sortPoints()
{
	std::vector<point2d> Group1;
	std::vector<point2d> Group2;
	std::vector<point2d> Group3;
	std::vector<point2d> Group4;

	int avgX = avarage(1);
	int avgY = avarage(2);

	for each (point2d var in points)
	{
		if (var.x > avgX && wh - var.y > avgY) Group1.push_back(var);
		if (var.x > avgX && wh - var.y < avgY) Group2.push_back(var);
		if (var.x < avgX && wh - var.y < avgY) Group3.push_back(var);
		if (var.x < avgX && wh - var.y > avgY) Group4.push_back(var);
	}

	sort(Group1, 1);
	sort(Group2, 2);
	sort(Group3, 3);
	sort(Group4, 4);

	points.clear();

	for each (point2d var in Group1)
		points.push_back(var);
	for each (point2d var in Group2)
		points.push_back(var);
	for each (point2d var in Group3)
		points.push_back(var);
	for each (point2d var in Group4)
		points.push_back(var);


}

void display()
{
	glFlush();
}

void myInit()
{
	glViewport(0, 0, ww, wh);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, (GLdouble)ww, 0.0, (GLdouble)wh);
	glMatrixMode(GL_MODELVIEW);
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glPolygonMode(GL_FRONT, GL_FILL);
	//glColor3f(1.0, 0.0, 0.0);
}

void myReshape(GLsizei w, GLsizei h)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, (GLdouble)w, 0.0, (GLdouble)h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	ww = w;
	wh = h;
}


void drawSquare(int x, int y)
{

	y = wh - y;
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLES);
	glVertex2f(x - sizee, y + sizee);
	glVertex2f(x + sizee, y + sizee);	
	glVertex2f(x , y - sizee);
	//glVertex2f(x - size, y - size);
	glEnd();
	glFlush();

}

void redrawPoints()
{
	for each (point2d var in points)
	{
		drawSquare(var.x, var.y);
	}
}


void draw(int ilsocKatow)
{
	sortPoints();

	myInit();

	glColor3f(0.0f, 0.0f, 0.0f);

	int ilosc = points.size();
	if (ilosc < ilsocKatow) { return; }
	int pelneWielokaty = ilosc / ilsocKatow;
	int pozostaleVertexy = ilosc % ilsocKatow;

	for (int i = 0; i < pelneWielokaty; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < ilsocKatow; j++)
		{
			glVertex2f(float(points[i*ilsocKatow+j].x), float(wh - points[i * ilsocKatow+j].y));
		}
		glEnd();
		glFlush();
	}
	if (pozostaleVertexy != 0)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < ilsocKatow; j++)
		{
			glVertex2f(float(points[ilosc- ilsocKatow  +j].x), float(wh - points[ilosc - ilsocKatow  + j].y));
		}
		glEnd();
		glFlush();
	}



}

void top_menu(int id)
{
	switch (id)
	{
	case 0:
		glClearColor(1.0, 1.0, 1.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);
		redrawPoints();
		break;
	case 1:
		glClearColor(1.0, 1.0, 1.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);
		points.clear();
		break;
	case 2:
		draw(3); 
		break;
	case 3:
		draw(4);
		break;	
	case 4:
		draw(5);
		break;
	case 5:
		draw(6);
		break;
	case 6:
		draw(7);
		break;
	case 7:
		draw(8);
		break;
	case 8:
		draw(9);
		break;
	case 9:
		draw(10);
		break;
	case 10:
		exit(0);
		break;
	
	}
}

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON&&state == GLUT_DOWN)
	{
		drawSquare(x, y);
		point2d tmp;
		tmp.x = x;
		tmp.y = y;
		points.push_back(tmp);
		//cout << "NowyPunkt " << x << "," << y << endl;
	}
	if (button == GLUT_RIGHT_BUTTON&&state == GLUT_DOWN)
		exit(0);
}



int main(int argc, char **argv)
{
	//int sub_menu;
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Zad5");
	glutDisplayFunc(display);

	myInit();
	glutMouseFunc(myMouse);
	//sub_menu = glutCreateMenu(size_menu);
	glutCreateMenu(top_menu);
	glutAddMenuEntry("Clear lines", 0);	
	glutAddMenuEntry("Clear all", 1);
	glutAddMenuEntry("3kat", 2);
	glutAddMenuEntry("4kat", 3);
	glutAddMenuEntry("5kat", 4);
	glutAddMenuEntry("6kat", 5);
	glutAddMenuEntry("7kat", 6);
	glutAddMenuEntry("8kat", 7);
	glutAddMenuEntry("9kat", 8);
	glutAddMenuEntry("10kat", 9);
	glutAddMenuEntry("Quit", 10);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	glutReshapeFunc(myReshape);
	glClear(GL_COLOR_BUFFER_BIT);
	glutMainLoop();
	return 0;
}