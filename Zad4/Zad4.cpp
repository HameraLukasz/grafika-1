#include "stdafx.h"
#include <GL/glut.h>
#include <stdlib.h>


// rozmiary bry�y obcinania

const GLdouble left = -10.0;
const GLdouble right = 10.0;
const GLdouble bottom = -10.0;
const GLdouble top = 10.0;
const GLdouble nearr = 50.0;
const GLdouble farr = 70.0;

// wsp�czynnik skalowania

GLfloat scale = 0.5;

// k�ty obrotu

GLfloat rotatex = 0.0;
GLfloat rotatey = 0.0;

// przesuni�cie

GLfloat translatex = 0.0;
GLfloat translatey = 0.0;

// funkcja generuj�ca scen� 3D

void Display()
{
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0, 0, -(nearr + farr) / 2);

	// przesuni�cie obiektu - ruch myszk�
	glTranslatef(translatex, translatey, 0.0);
	glScalef(scale, scale, scale);

	//rotatex += 10;

	glRotatef(rotatex, 1.0, 5.0, 0);

	glColor3f(0.0, 0.0, 0.0);

	// rysowanie obiektu
	//glutWireSphere(1.0, 20, 10);

	int windowWidth, windowHeight, x1, y1;
	windowWidth= windowHeight= x1= y1 = 0;
	glBegin(GL_TRIANGLES);

	glVertex2f(15.64f, 12.47);
	glVertex2f(19.5, -4.45);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(19.5, -4.45);
	glVertex2f(8.68, -18.02);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(8.68, -18.02);
	glVertex2f(-8.68, -18.02);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(-8.68, -18.02);
	glVertex2f(-19.5, -4.45);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(-19.5, -4.45);
	glVertex2f(-15.64, 12.47);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(-15.64, 12.47);
	glVertex2f(0, 20);
	glVertex2f(0.0f, 0.0f);

	glVertex2f(0, 20);
	glVertex2f(15.64f, 12.47);
	glVertex2f(0.0f, 0.0f);

	glEnd();


	// skierowanie polece� do wykonania
	glFlush();

	// zamiana bufor�w koloru
	glutSwapBuffers();
}

// zmiana wielko�ci okna

void Reshape(int width, int height)
{
	// obszar renderingu - ca�e okno
	glViewport(0, 0, width, height);

	// wyb�r macierzy rzutowania
	glMatrixMode(GL_PROJECTION);

	// macierz rzutowania = macierz jednostkowa
	glLoadIdentity();

		glFrustum(left, right, bottom, top, nearr, farr);

	// generowanie sceny 3D
	Display();
}

void TimerFunction(int value) {
	rotatex += 1;
	glutPostRedisplay();
	glutTimerFunc(33, TimerFunction, 1);
}

int main(int argc, char * argv[])
{
	// inicjalizacja biblioteki GLUT
	glutInit(&argc, argv);

	// inicjalizacja bufora ramki
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	// rozmiary g��wnego okna programu
	glutInitWindowSize(400, 400);
	glutCreateWindow("Zad4");

	glutDisplayFunc(Display);
	glutTimerFunc(33, TimerFunction, 1);

	glutReshapeFunc(Reshape);
	// wprowadzenie programu do obs�ugi p�tli komunikat�w
	glutMainLoop();
	return 0;
}